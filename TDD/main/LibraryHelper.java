package org.tzjo.tdd;

/**
 * My library helper application
 *
 */
public class LibraryHelper 
{
	private String books[][];
	private String users[][];
	public LibraryHelper()
	{
		books = new String[50][6];
		users = new String[50][5];
	}
	public boolean addBook(int ID_book, String author, String title, String publishing, String publishing_year)
	{
		try {
			books[ID_book][0] = author;
			books[ID_book][1] = title;
			books[ID_book][2] = publishing;
			books[ID_book][3] = publishing_year;
			return true;
		}
		catch(Exception e1) {
			return false;
		}
	}
	public boolean deleteBook(int ID_book)
	{
		try {
			books[ID_book][0] = null;
			books[ID_book][1] = null;
			books[ID_book][2] = null;
			books[ID_book][3] = null;
			return true;
		}
		catch(Exception e1) {
			return false;
		}
	}
	public boolean editBook(int ID_book, String author, String title, String publishing, String publishing_year)
	{
		try {
			if(ID_book != 0) {
				if(author != null) {
					books[ID_book][0] = author;
				}
				if(title != null) {
					books[ID_book][1] = title;			
				}
				if(publishing != null) {
					books[ID_book][2] = publishing;		
				}
				if(publishing_year != null){
					books[ID_book][3] = publishing_year;	
				}

				return true;
				
			}
			else return false;
			
		}
		catch(Exception e1) {
			return false;
		}
	}
	public boolean addUser(int ID_user, String name, String lastname, String birthday, String gender, String address)
	{
		try {
			books[ID_user][0] = name;
			books[ID_user][1] = lastname;
			books[ID_user][2] = birthday;
			books[ID_user][3] = gender;
			books[ID_user][4] = address;
			return true;
		}
		catch(Exception e1) {
			return false;
		}
	}
	public boolean deleteUser(int ID_user)
	{
		try {
			books[ID_user][0] = null;
			books[ID_user][1] = null;
			books[ID_user][2] = null;
			books[ID_user][3] = null;
			books[ID_user][4] = null;
			return true;
		}
		catch(Exception e1) {
			return false;
		}
	}
    public static void main( String[] args )
    {
        LibraryHelper help = new LibraryHelper();
        help.editBook(1, null, null, "aa", "ddad");
    }
}
