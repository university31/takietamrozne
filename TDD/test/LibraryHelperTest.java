package org.tzjo.tdd;

import org.junit.Test;

import junit.framework.Assert;

/**
 * Unit test for our library helper.
 */
public class LibraryHelperTest {
	
	@Test
    public void AddBookTest()
    {
    	LibraryHelper libhelp = new LibraryHelper();
    	int ID_book;
    	String author, title, publishing, publishing_year;
    	ID_book = 1;
    	author = "H. Sienkiewicz";
    	title = "Potop";
    	publishing = "Nowa Era";
    	publishing_year = "2010";
    	//check the author is String type
        Assert.assertTrue(author instanceof String);
    	//check the title is String type
        Assert.assertTrue(title instanceof String);
    	//check the publishing house is String type
        Assert.assertTrue(publishing instanceof String);
    	//check the publishing year is Integer type
        Assert.assertTrue(publishing_year instanceof String);
        //check the method return true
        Assert.assertTrue(libhelp.addBook(ID_book, author, title, publishing, publishing_year));
    }
	@Test
    public void DeleteBookTest()
    {
		LibraryHelper libhelp = new LibraryHelper();
    	int ID_book;
    	ID_book = 1;
        //check the method return true
        Assert.assertTrue(libhelp.deleteBook(ID_book));
    }
	@Test
    public void EditBookTest()
    {
		LibraryHelper libhelp = new LibraryHelper();
    	int ID_book;
    	String author, title, publishing, publishing_year;
    	ID_book = 1;
    	author = null;
    	title = null;
    	publishing = "Stara Era";
    	publishing_year = "2015";
        //check the method return true
        Assert.assertTrue(libhelp.editBook(ID_book, author, title, publishing, publishing_year));
    }
	@Test
    public void AddUserTest()
    {
    	LibraryHelper libhelp = new LibraryHelper();
    	int ID_user;
    	String name, lastname, birthday, gender, address;
    	ID_user = 1;
    	name = "John";
    	lastname = "Devoe";
    	birthday = "1999";
    	gender = "male";
    	address = "ul. Jana Pawła II 12, Koszalin";
        //check the method return true
        Assert.assertTrue(libhelp.addUser(ID_user, name, lastname, birthday, gender, address));
    }
	@Test
    public void DeleteUserTest()
    {
		LibraryHelper libhelp = new LibraryHelper();
    	int ID_user;
    	ID_user = 1;
        //check the method return true
        Assert.assertTrue(libhelp.deleteUser(ID_user));
    }
}
